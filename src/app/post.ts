export class Post {
    title : string;
    content: string;
    loveIts: number;
    created_at: Date;
    constructor(title, content, loveIts) {
      this.title = title;
      this.content = content;
      this.loveIts = loveIts;
      this.created_at = new Date();
    }
    get_color() {
      if (this.loveIts > 0)
          return true
      if (this.loveIts < 0)
          return false
      else
          return null
    }
  }
  